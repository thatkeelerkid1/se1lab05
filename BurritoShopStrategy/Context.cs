﻿using System;

namespace BurritoShopStrategy
{
    public class Context
    {
        private ICookingStrategy _strat;

        public Context()
        {
        }

        public Context(ICookingStrategy strat)
        {
            this._strat = strat;
        }

        public void SetStrategy(ICookingStrategy strat)
        {
            this._strat = strat;
        }

        public Burrito BuildBurrito()
        {
            Burrito burrito = this._strat.Cook();
            return burrito;
        }


    }
}