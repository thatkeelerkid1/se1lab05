﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BurritoShopStrategy
{
    public class Burrito
    {
        public List<string> Toppings { get; set; }
        public string MeatType { get; set; }
        public string MeatTemp { get; set; }
        public Burrito()
        {
            Toppings = new List<string>();
        }
        public Burrito(List<string> toppings, string meatType, string meatTemp)
        {
            Toppings = toppings;
            MeatType = meatType;
            MeatTemp = meatTemp;
        }
        

        public override string ToString()
        {
            string info;
            info = $"Meat Type: \n\t{MeatType} \nMeat Temp: \n\t{MeatTemp} \nToppings: ";
            foreach(var x in Toppings)
            {
                info += "\n\t" + x;
            }
            return info;
        }
    }
}
