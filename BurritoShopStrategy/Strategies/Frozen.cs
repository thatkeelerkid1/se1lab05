﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BurritoShopStrategy
{
    class Frozen : ICookingStrategy
    {
        public Burrito Cook()
        {
            Burrito burrito = new Burrito();
            burrito.MeatTemp = "30°C";    //Assigning Temperature to 30 since it is FROZEN
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("\n\t\t\tBUILDING BURRITO"+"\n\t\t\t________________"+"\nEnter Meat Type");
            Console.ForegroundColor = ConsoleColor.Blue;
            burrito.MeatType = Console.ReadLine();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("\nEnter Toppings followed by a ','");
            Console.ForegroundColor = ConsoleColor.Blue;
            burrito.Toppings = Console.ReadLine().Split(',').ToList();
            return burrito;
        }
    }
}
