﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Project:		BurritoShopStrategy
//	File Name:		Oven.cs
//	Description:	Module to simulate cooking a burrito using an oven
//	Author:			Tyler Simpson, simpsontd@etsu.edu
//	Created:		Tuesday, February 28, 2020
//	Copyright:		Tyler Simpson, 2020
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BurritoShopStrategy
{
    /// <summary>
    /// Virtual oven to bake a virtual burrito for a user to eat to bits
    /// </summary>
    class Oven : ICookingStrategy
    {
        /// <summary>
        /// Tracks time left on timer
        /// </summary>
        public int Timer { get; set; }

        /// <summary>
        /// Tracks burritos in oven
        /// </summary>
        public List<Burrito> BurritoInOven { get; set; }

        /// <summary>
        /// Cook a given burrito
        /// </summary>
        /// <param name="burrito"></param>
        /// <returns></returns>
        public Burrito Cook()
        {
            Burrito burrito = new Burrito(); //new burrito object for a grilled burrito

            burrito.MeatTemp = "190°C";
            this.Preheat(burrito.MeatTemp);
            //get toppings and meat type from user
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("\n\t\t\tBUILDING BURRITO" + "\n\t\t\t________________" + "\nEnter Meat Type");
            Console.ForegroundColor = ConsoleColor.Blue;
            burrito.MeatType = Console.ReadLine();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("\nEnter list of toppings separated by ','");
            Console.ForegroundColor = ConsoleColor.Blue;
            burrito.Toppings = Console.ReadLine().Split(',').ToList();
            this.LoadItem(burrito);
            this.SetTimer(15);
            while (Timer > 0)
            {
                Timer--;    //Used to help test program since actual timer has not been implemented yet.
            }
            burrito = this.UnloadItem(burrito);

            return burrito;
        }

        /// <summary>
        /// Preheat the oven to given temperature
        /// </summary>
        /// <param name="temp"></param>
        public void Preheat(string temp)
        {
            //set temp
        }

        /// <summary>
        /// Set timer on oven
        /// </summary>
        /// <param name="minutes"></param>
        public void SetTimer(int minutes)
        {
            //start timer
        }

        /// <summary>
        /// put burrito in oven
        /// </summary>
        /// <param name="Item"></param>
        /// <returns></returns>
        public Burrito LoadItem(Burrito burritoInOven)
        {
            //take items out of item
            return burritoInOven;
        }

        /// <summary>
        /// Take burrito out of oven
        /// </summary>
        /// <param name="Item"></param>
        /// <returns></returns>
        public Burrito UnloadItem(Burrito burritoInOven)
        {
            //take items out of item
            return burritoInOven;
        }
    }
}
