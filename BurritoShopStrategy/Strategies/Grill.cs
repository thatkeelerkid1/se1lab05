﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BurritoShopStrategy //
{
    class Grill : ICookingStrategy //extends cooking strategy
    {
        public Burrito Cook()
        {
            Burrito burrito = new Burrito(); //new burrito object for a grilled burrito

            burrito.MeatTemp = "72°C";    //Assigning Temperature to 72 since it is room temperature

            //get toppings and meat type from user
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("\n\t\t\tBUILDING BURRITO" + "\n\t\t\t________________" + "\nEnter Meat Type");
            Console.ForegroundColor = ConsoleColor.Blue;
            burrito.MeatType = Console.ReadLine();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("\nEnter Toppings followed by a ','");
            Console.ForegroundColor = ConsoleColor.Blue;

            //process toppings and meat type and return completed burrito to the cooking strategy
            burrito.Toppings = Console.ReadLine().Split(',').ToList();
            return burrito;
        }
    }
}
