﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BurritoShopStrategy
{
    class Microwave : ICookingStrategy  //extends cooking strategy
    {
        public Burrito Cook()
        {
            Burrito burrito = new Burrito();

            burrito.MeatTemp = "100°C";    //Temp at 100 because degree at which microwave heats

            //get toppings and meat type from user
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("\n\t\t\tBUILDING BURRITO" + "\n\t\t\t________________" + "\nEnter Meat Type");
            Console.ForegroundColor = ConsoleColor.Blue;
            burrito.MeatType = Console.ReadLine();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("\nEnter Toppings followed by a ','");
            Console.ForegroundColor = ConsoleColor.Blue;

            //Process toppings and meat type and return completed burrito to the cooking strategy
            burrito.Toppings = Console.ReadLine().Split(',').ToList();
            return burrito;
        }
    }
}
