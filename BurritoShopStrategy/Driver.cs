﻿using System;

namespace BurritoShopStrategy
{
    /// <summary>
    /// Driver Class
    /// </summary>
    class Driver
    {
        /// <summary>
        /// Main method
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            var context = new Context();
            Utility.Menu();
            Console.Write("CHOICE: ");
            Console.ForegroundColor = ConsoleColor.Blue;
            int choice = Convert.ToInt32(Console.ReadLine());
            Console.Clear();
            Burrito myBurrito = new Burrito();
            switch (choice)
            {
                default:
                    break;
                case 1:
                    context.SetStrategy(new Frozen());
                    myBurrito = context.BuildBurrito();
                    break;
                case 2:
                    context.SetStrategy(new Grill());
                    myBurrito = context.BuildBurrito();
                    break;
                case 3:
                    context.SetStrategy(new Microwave());
                    myBurrito = context.BuildBurrito();
                    break;
                case 4:
                    context.SetStrategy(new Oven());
                    myBurrito = context.BuildBurrito();
                    break;

            }//END Switch
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("\t\t\t\tBurrito is READY FOR PICKUP!");
            Console.WriteLine("\t\t\t\t____________________________\n");
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine(myBurrito.ToString());
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("\n\nPress any key to exit the program");
            Console.ReadKey();
       }//END Main
    }//END Driver
}//END Namespace
