﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BurritoShopStrategy
{
    public interface ICookingStrategy
    {
        Burrito Cook();
    }
}
